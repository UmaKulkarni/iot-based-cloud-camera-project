#include <aws/core/Aws.h>
#include <aws/s3/S3Client.h>
#include <aws/s3/model/CreateBucketRequest.h>
#include <aws/s3/model/PutObjectRequest.h>
#include <aws/core/utils/UUID.h>
#include <fstream>
#include <iostream>
bool AwsDoc::S3::CreateBucket(const Aws::String& bucketName, 
    const Aws::S3::Model::BucketLocationConstraint& region)
{
	Aws::S3::S3Client s3_client;

    Aws::S3::Model::CreateBucketRequest request;
    request.SetBucket(bucketName);

  
    if (region != Aws::S3::Model::BucketLocationConstraint::us_east_1)//set region
    {
        Aws::S3::Model::CreateBucketConfiguration bucket_config;
        bucket_config.SetLocationConstraint(region);

        request.SetCreateBucketConfiguration(bucket_config);
    }

    Aws::S3::Model::CreateBucketOutcome outcome = 
        s3_client.CreateBucket(request);

    if (!outcome.IsSuccess())
    {
        auto err = outcome.GetError();
        std::cout << "Error " <<
            err.GetExceptionName() << ": " << err.GetMessage() << std::endl;

        return false;
    }

    return true;
}


bool AwsDoc::S3::PutObject(const Aws::String& bucketName, 
    const Aws::String& objectName,
    const Aws::String& region)
{
    struct stat buffer;

    if (stat(objectName.c_str(), &buffer) == -1)
    {
        std::cout << "Error" <<
            objectName << "NO" << std::endl;

        return false;
    }

    Aws::Client::ClientConfiguration config;

    if (!region.empty())
    {
        config.region = region;
    }

    Aws::S3::S3Client s3_client(config);
    
    Aws::S3::Model::PutObjectRequest request;
    request.SetBucket(bucketName);
    request.SetKey(objectName);
 

    Aws::S3::Model::PutObjectOutcome outcome = 
        s3_client.PutObject(request);

    if (outcome.IsSuccess()) {

        std::cout << "object done '" << objectName << "' to bucket '"
            << bucketName << "'.";
        return true;
    }
    else 
    {
        std::cout << "Error " << 
            outcome.GetError().GetMessage() << std::endl;
       
        return false;
    }
}

int main()
{
	short option;
    Aws::SDKOptions options;
    Aws::InitAPI(options);
    {
    
    	
    	  Aws::S3::Model::BucketLocationConstraint region =
            Aws::S3::Model::BucketLocationConstraint::us_east_1;

        
        Aws::String uuid = Aws::Utils::UUID::RandomUUID();
        Aws::String bucket_name = "my-bucket-" + //bucket name
            Aws::Utils::StringUtils::ToLower(uuid.c_str());
    	
    	do
		{
			std::cout<<"Enter the options 1.Create Bucket 2.Add data into the bucket"<<std::endl;
	    	switch(options)
	    	{
	    		case 1:
	    			// Create the bucket.
			        if (AwsDoc::S3::CreateBucket(bucket_name, region))
			        {
			            std::cout << "Created bucket " << bucket_name <<" in the specified AWS Region." << std::endl;
			        }
			        else
			        {
			        	std::cout<<"cant create a bucket"<<std::endl;
			            return 1;
			        }
			        break;
	
				case 2:
                                       
                        s3.bucket(bucket_name).object('key').upload_file("/home/gitproject/test.mp4");//path

                                      if (!AwsDoc::S3::PutObject(bucket_name, object_name, region)) 
					{
			            return 1;
			        }
			        
			        else
			        {
						std::cout<<"your file "<<object_name<<" is uploaded successfully"<<std::endl;
					}
					
					break;
				
			
			}
		
			
			
			std::cout<<"do you want to continue? Press 1 to Yes and any key to exit.. "<<endl;
			std::cin>>option;
			
		}while(option==1);
		
    
    }
    ShutdownAPI(options);

	return 0;
}

