#include<iostream.h>
#include<wiringPi.h>//GPIO interface library for Raspberry pi

// For Digital Output when sound intensity reaches threshold level
int led = 13; //defining LED as interface 
int button_pin = 3; //define Digital Output sensor as interface
int val ;
// For Analog Output, real-time output voltage signal of the microphone
int sensor_pin = a5;//select the input pin for the potentiometer
int sensor_value = 0;

void setup()
{
pinMode(led, OUTPUT); // initialize the LED  as output
pinmode(button_pin , INPUT); // initialize Digital Output sensor as input
serial.Begin(9600);//sets the baud rate(analog)
}

void loop()
{
val = digitalRead(button_pin);// assigned a value 3 to read val
if ( val == high){
digitalWrite(led, HIGH);   // turn the LED on (HIGH is the voltage level)
}
else{         
digitalWrite(led, LOW); // turn the LED off by making the voltage LOW
}

sensor_value = analogRead(sensor_pin);
digitalWrite(led, HIGH);
delay(sensor_value);
digitalWrite(led, LOW);
delay(sensor_value);
Serial.println(sensor_value);
}

int main(void)//main function
{
	if(wiringPiSetup()<0){
		printf("setup wiring pi failed.");
		return 1;
	}
setup();//calling setup function
loop();//calling  loop function
	
return 0;
}



